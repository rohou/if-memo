/*
 */

#include <stdio.h>

#include "debug.h"

FILE* debug = NULL;

#if defined(mem_debug)

void init_debug()
{
  debug = fopen(debug_file, "w");
  if (!debug) {
    fprintf(stderr, "Could not open debug file '%s'\n", debug_file);
  }
}

void DEBUG_PRINTF(const char* format, va_list ap)
{
  if (debug) {
    vfprintf(debug, format, ap);
    fflush(debug);
  }
}

#endif  /* mem_debug */
