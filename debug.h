#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>


#if defined(mem_debug)

#define debug_file "if-memo.debug"

extern FILE* debug;

void init_debug(void);
void DEBUG_PRINTF();


#else
#define DEBUG_PRINTF(...)


#endif  /* mem_debug */

#endif  /* DEBUG_H */
