
#define FUN_ID strings(F_, m_fun)
#define _i       funs[FUN_ID].i
#define _hit     funs[FUN_ID].hit
#define _name    funs[FUN_ID].name
#define _gotaddr funs[FUN_ID].gotaddr
#ifndef call_string
#define call_string ((double(*)(double)) funs[FUN_ID].real_func)(arg)
#endif

double m_fun(double arg)
{
#if !defined(mem_actualtime) && defined(mem_stop)
  /* FIXME: do this outside the critical part, and single thread! */
  if (_i == 0) {
    long int ret = ((long int) __builtin_return_address(0));
    long int pltaddr = ret + *(int*)(ret - 4);
    _gotaddr = (int*) (pltaddr + 6 + *(int*) (pltaddr + 2));
  }
#endif  /* !mem_actualtime && mem_stop */

#ifdef mem_time
#include "timecounter_start.c"
#endif

  double ret;

#ifdef mem_actualtime
  ret = call_string;
#include "timecounter_stop.c"
  return ret;
#endif

  union a_union {
    long   i;
    double f;
  } u;
  u.f = arg;
  long index = u.i;

#ifdef mem_noasm
  //  index = *(int*)((int *)&arg) ^ *((int*)&arg + 1);
  index = (index & 0xffffffff) ^ (index >> 32);
  index = index ^ (index >> 16);
#else
#include "asm/double_index.asm"
#endif

  index &= mem_flag;
  if (strings(table_, m_fun) [index][0] == arg) {

#if defined(mem_dump) || defined(mem_stop) || defined(mem_debug)
    _hit++;
#endif

    ret = strings(table_, m_fun) [index][1];

#ifdef mem_hittime
#include "timecounter_stop.c"
#endif

    return ret;
  }

#if defined(mem_dump) || defined(mem_stop) || defined(mem_debug)
  _i++;
#endif

  strings(table_, m_fun) [index][0] = arg;
  strings(table_, m_fun) [index][1] = call_string;
  ret = strings(table_, m_fun)[index][1];

#ifdef mem_misstime
#include "timecounter_stop.c"
#endif
  
  return ret;
}
#undef call_string
