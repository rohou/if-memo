
asm volatile(
 "CPUID\n\t"
 "RDTSC\n\t"
 "mov %0, edx\n\t"
 "mov %1, eax \n\t"
 :"=r" (cycles_high), "=r" (cycles_low)
 :
 :"%rax", "%rbx", "%rcx", "%rdx");
