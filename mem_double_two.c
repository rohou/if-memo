
#define FUN_ID strings(F_, m_fun)
#define _i       funs[FUN_ID].i
#define _hit     funs[FUN_ID].hit
#define _name    funs[FUN_ID].name
#define _gotaddr funs[FUN_ID].gotaddr
#ifndef call_string
#define call_string ((double(*)(double, double)) funs[FUN_ID].real_func)(arg1,arg2)
#endif


double m_fun(double arg1, double arg2)
{
#ifndef mem_actualtime
#ifdef mem_stop
  if (_i == 0) {
    long int ret = ((long int) __builtin_return_address(0));
    long int pltaddr = ret + *(int*) (ret - 4);
    _gotaddr = (int*) (pltaddr + 6 + *(int*) (pltaddr + 2));
  }
#endif
#endif

#ifdef mem_time
#include "timecounter_start.c"
#endif

  double ret;

#ifdef mem_actualtime
  ret = call_string;
#include "timecounter_stop.c"
  return ret;
#endif

  long index;
  
#ifdef mem_noasm
  union a_union {
    long   i;
    double f;
  } u;
  u.f = arg1;
  long arg1_i = u.i;
  u.f = arg2;
  long arg2_i = u.i;
  
  index = (arg1_i & 0xffffffff) ^ (arg1_i >> 32) ^
          (arg2_i & 0xffffffff) ^ (arg2_i >> 32);
  index = index ^ (index>>16);
#else
#include "asm/double_double_index.asm"
#endif

  index &= mem_flag;

  if (strings(table_, m_fun) [index][0] == arg1 &&
      strings(table_, m_fun) [index][1] == arg2)   {

#if defined(mem_dump) || defined(mem_stop) || defined(mem_debug)
    _hit++;
#endif

    ret = strings(table_, m_fun) [index][2];

#ifdef mem_hittime
#include "timecounter_stop.c"
#endif

    return ret;
  }

#if defined(mem_dump) || defined(mem_stop) || defined(mem_debug)
  _i++;
#endif

  strings(table_, m_fun) [index][0] = arg1;
  strings(table_, m_fun) [index][1] = arg2;
  strings(table_, m_fun) [index][2] = call_string;
  ret = strings(table_, m_fun)[index][2];

#ifdef mem_misstime
#include "timecounter_stop.c"
#endif

  return ret;
}
#undef call_string
