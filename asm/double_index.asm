// double dum1, dum2, dum3;
asm(".intel_syntax noprefix");
asm volatile(
     "movq %0, %1 \n"
     "pinsrq xmm1, %0, 0 \n"
     "pshuflw xmm2, xmm1, 0x1b \n" // byte-wise reverses xmm1 and stores in xmm2
     "pxor xmm2, xmm1 \n"
     "pshuflw xmm1, xmm2, 0x41 \n" // ABCD in xmm2 is made to CDDC in xmm1
     "pxor xmm1, xmm2 \n"
     "pextrw %0, xmm1, 0\n"
             : "=r" (index)
             : "r"(arg)
             :"xmm1", "xmm2"
          );

