//double dum1, dum2, dum3;
int dum1;
asm(".intel_syntax noprefix");
asm volatile(
		"pmovzxdq xmm2, xmm0 \n\t"
		"pxor xmm2, xmm1 \n\t"
                "movd %0, xmm2 \n\t"
                "mov %1, %0 \n\t"
		"sar %1, 16 \n\t"
		"xor %0, %1 \n\t"		
		: "=r" (index), "=r"(dum1)
                : 
                :"xmm0", "xmm1", "xmm2"
                );

