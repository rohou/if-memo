asm(".intel_syntax noprefix");
asm volatile(
                "movq rdx, xmm0 \n\t"
                "pinsrq xmm0, rdx, 1 \n\t"

                "pshuflw xmm2, xmm0, 0x1b \n\t"//byte-wise reverses the content of xmm0 and stores in xmm2
                "pxor xmm2, xmm0 \n\t"
                "pshuflw xmm3, xmm2, 0x41 \n\t" // ABCD in xmm3 is made to CDDC in xmm2
                "pxor xmm2, xmm3 \n\t"
                //"mov  rdx, xmm2\n\t" //index now in rdx
                "pextrw rdx, xmm2, 0\n\t"
		"mov %0, edx \n\t"
                "shl rdx, 6 \n\t"
//              "movq rdx, 0 \n\t"      



                "vmovapd xmm1,  [%3+rdx] \n\t"
                "pcmpeqq xmm1, xmm0 \n\t"
                "pmovmskb rbx, xmm1 \n\t"
                "popcnt rcx, rbx \n\t"
                "jz "str(m_fun)"firstmiss\n\t"

                "bsf rcx, rbx \n\t"
		"add rdx, rcx \n\t" //moves rdx to the 
		//Hit in table return the value
	//	"vmovapd xmm0, [%1+rdx+32]\n\t"
		//"ret \n\t"
		"mov %1, 1 \n\t"
		"movq %2, [%3+rdx+32]\n\t"
                "jmp "str(m_fun)"end \n\t"

                str(m_fun)"firstmiss:\n\t"
                "vmovapd xmm1,  [%3+rdx+16] \n\t"
                "pcmpeqq xmm1, xmm0 \n\t"
                "pmovmskb rbx, xmm1 \n\t"
                "popcnt rcx, rbx \n\t"
                "jz "str(m_fun)"end\n\t"

                "bsf rcx, rbx \n\t"
                "add rcx, 16 \n\t"
		"add rdx, rcx \n\t"
		//Hit in table return the value
	//	"vmovapd xmm0, [%1+rdx+32] \n\t"
		"mov %1, 1 \n\t"
		"movq %2, [%3+rdx+32]\n\t"
                "jmp "str(m_fun)"end \n\t"
		//"ret \n\t"
                

		"mov %1, 1\n\t"
                str(m_fun)"end:\n\t"
              //  "movq %0, -1 \n\t"
		: "=r" (val), "=r"(hit), "=r"(res)
                : "r"(q)
                :"xmm0","xmm1","rdx","rbx","rcx","xmm2","xmm3"
                );

