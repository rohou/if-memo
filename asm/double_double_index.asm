// double dum1, dum2, dum3;
long dum1;
asm(".intel_syntax noprefix");
asm volatile(
	"movq %0, %2 \n\t"
	"movq %1, %3 \n\t"
	"xor %0, %1 \n\t"
        "pinsrq xmm4, %0, 0 \n\t"
        "pshuflw xmm2, xmm4, 0x1b \n\t" // byte-wise reverses xmm4, stores in xmm2
        "pxor xmm2, xmm4 \n\t"
        "pshuflw xmm3, xmm2, 0x41 \n\t" // ABCD in xmm3 is made to CDDC in xmm2
        "pxor xmm3, xmm2 \n\t"
        "pextrw %0, xmm3, 0\n\t"
        // "mov %0, edx \n\t"
		: "=r" (index), "=r"(dum1)
                :"r"(arg1), "r"(arg2) 
                : "xmm2", "xmm3", "xmm4"
                );

