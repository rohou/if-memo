
  asm volatile(
          "RDTSCP\n\t"
          "mov %0, edx \n\t"
          "mov %1, eax \n\t"
          "CPUID\n\t"
          : "=r" (cycles_high1), "=r" (cycles_low1)
          :
          :"%rax", "%rbx", "%rcx", "%rdx");

  start = (((long) cycles_high  << 32) | cycles_low);
  end =   (((long) cycles_high1 << 32) | cycles_low1);

  /* FIXME: not thread-safe */
  if (funs[FUN_ID].mem_ti == timexsize) {
    timeprint(&funs[FUN_ID]);
    funs[FUN_ID].mem_ti = 0;
    funs[FUN_ID].mem_time_flag = 1;
  }

  funs[FUN_ID].timex[funs[FUN_ID].mem_ti++] = end - start;
