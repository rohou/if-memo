CFLAGS=-Wall -fpic -masm=intel -mtune=native -O2 # -O0 -g 

# Possible defines - edit as you wish

# CFLAGS+=-Dmem_stop   # Activates the stopper thread
# CFLAGS+=-Dmem_debug  # Dumps debug info.
# CFLAGS+=-Dmem_actualtime
# CFLAGS+=-Dmem_time
# CFLAGS+=-Dmem_dump
# CFLAGS+=-Dmem_noasm # Use C code for index hashing when asm not available


package = if-memo
version = 0.1
tarname = if-memo
distdir = $(tarname)-$(version)



all: if-memo.so

if-memo.so: alllibm.o debug.o
	$(CC) -shared -o if-memo.so alllibm.o debug.o -lm -lpthread -ldl

alllibm.o: alllibm.c interceptors.c funs.def mem_double.c mem_double_two.c mem_float.c mem_float_two.c debug.h
	$(CC) $(CFLAGS) -c alllibm.c

debug.o: debug.c debug.h
	$(CC) $(CFLAGS) -c debug.c

# interceptors.o: interceptors.c
# 	$(CC) $(CFLAGS) -c interceptors.c

interceptors.c: gen.c funs.def
	$(CPP) gen.c | \
	       sed "s/CR /\n/g;s/include/#include/;s/define/#define/;s/undef/#undef/" > interceptors.c


clean:
	/bin/rm -f interceptors.c interceptors.o alllibm.o if-memo.so

dist:
	mkdir -p $(distdir)
	cp alllibm.c  debug.c  debug.h  def.h  gen.c  mem_double.c  \
           mem_double_two.c  mem_float.c  mem_float_two.c  funs.def \
	   timecounter_start.c timecounter_stop.c		    \
           Makefile README                                          \
                    $(distdir)
	cp -r asm $(distdir)
	tar cvjf $(tarname).tar.bz2 $(distdir)
