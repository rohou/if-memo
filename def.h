#define timexsize 10000

#define table_size (65536)  /* power of 2 */
#define mem_flag (table_size - 1)


#define ass_set 1

#if defined(mem_actualtime) || defined(mem_hittime) || defined(mem_misstime)
#define mem_time 1
#endif
  
#ifdef mem_stop
#define mem_stop_int 100      // interval to check for stopping (milliseconds)
#define mem_restart_time 1000 // time (sec) after which memoization is restarts
#define mem_stop_init_time 3  // time after which helper thread starts (seconds)
#endif  /* mem_stop */
