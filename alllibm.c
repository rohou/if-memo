/*
 * This file is part of if-memo.
 *
 * Authors: Arjun SURESH, Erven ROHOU
 * Copyright (c) 2016 Inria.
 *
 * If-memo is registered at the APP (Agence de Protection des
 * Programmes) under number IDDN.FR.001.250013.000.S.P.2015.000.10800.
 */

#define _GNU_SOURCE 1

#include <dlfcn.h>
#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "debug.h"

#define DEF_REQ_HIT_RATE(a, b)   /* empty */

#define stringify(str1, str2) str1 ## str2
#define strings(str1,str2) stringify(str1, str2)
#define str1(x) #x
#define str(x)  str1(x)

#include "def.h"


/* Create an enum type for all functions to be memoized */
#define DEF(f, n, t, template) strings(F_, f),
enum {
#include "funs.def"
#undef DEF
  F_MAX  /* last and unused value */
};


/* Main structure containing all relevant information about memoized
   functions. */
struct fun_t {
  char* name;
  void* real_func;  /* type of real_func?? */
  long  i, hit;     /* access and hit counters */

#ifdef mem_time
  unsigned long timex[timexsize];
  unsigned long mem_ti;
  int mem_time_flag;
  FILE*  dumptime;
#endif  /* mem_time */

#ifdef mem_stop    /* within ifdef mem_stop? */
  unsigned long orig;  /* TYPE??? */
  unsigned long prev1, prev2;
  unsigned long scount;
  int*          gotaddr;
  int   stopped;
  float reqd_hit;  /* FIXME initialize how */
#endif  /* mem_stop */
};


/* Instantiation of the struct for all interesting functions.  We
   initialize the name here, all other fields are zeroed.
*/
#define DEF(f, n, t, template) { str(f) },
struct fun_t funs[] = {
#include "funs.def"
};
#undef DEF


/* Define lookup tables */
#define DEF(f, n, typ, z) __thread typ strings(table_, f)[table_size*ass_set][n+1] __attribute__ ((aligned (64)));
#include "funs.def"
#undef DEF



#ifdef mem_time
unsigned int cycles_high, cycles_low, cycles_high1, cycles_low1;
unsigned long start, end;

void timeprint(struct fun_t* f)
{
  fprintf(stderr, "timeprint(%s)\n", f->name);
  if (f->mem_time_flag == 0) {
    int printi;
    for(printi = 0; printi < f->mem_ti; printi++) {
      fprintf(f->dumptime, "%lu\n", f->timex[printi]);
    }
    fclose(f->dumptime);
  }
}

#endif  /* mem_time */


/* This file includes all the templates */
#include "interceptors.c"



void fillarg()
{
  int i, j;

#define DEF(f, n, t, template)                  \
  for(j=0; j < n; j++) {                        \
    strings(table_, f)[i][j] = NAN;             \
  }

  for (i = 0; i < table_size*ass_set; i++) {
#include "funs.def"
  }
#undef DEF
}



#if !defined(mem_actualtime) && defined(mem_stop)

static pthread_t thread;  /* helper thread */

void stopit(struct fun_t* f)
{
  /* If memoization is not stopped and hit rate is below threshold */
  if ((!f->stopped) && (f->i > f->reqd_hit * f->hit)) {
    fprintf(stderr, "%s: stopping memoization , miss = %lu   hit = %lu\n",
            f->name, f->i, f->hit);

    fprintf(stderr, "Stopping for '%s'\n", f->name);
    *(long int*)(f->gotaddr) = f->orig;  /* stop it: restore address in GOT */
    fprintf(stderr, "Stopped\n");

    f->stopped = 1;
  }
  else if (!f->stopped && f->i > 0) {

    fprintf(stderr, "%s: not stopped, miss = %lu hit = %lu   req_hit = %f\n",
            f->name, f->i, f->hit, f->reqd_hit);

    f->prev2 = f->i - f->prev1;
    f->prev1 = f->i;
  }
  else if (f->i == 0) {
    fprintf(stderr, "%s: i == 0\n", f->name);
    f->stopped = 1;
  }
}

void restart(struct fun_t* f)
{
  if (f->stopped) {
    f->scount++;
    if (f->scount == ((float)mem_restart_time / mem_stop_int) * 1000) {
      *(long int*)f->gotaddr = (long)dlsym(RTLD_DEFAULT, f->name);
      f->scount = 0;
      f->stopped = 0;
    }
  }
}

void* helper(void * arg)
{
  int f;

  sleep(mem_stop_init_time);

  for(f=0; f < F_MAX; f++) {
    funs[f].orig = (long)dlsym(RTLD_NEXT, funs[f].name);
    funs[f].prev1 = 0;
    funs[f].prev2 = 0;
    funs[f].scount = 0;
    funs[f].stopped = 0;
  }

  /* initializa required hit rates from file */
#define DEF(a, b, c, d)
#undef DEF_REQ_HIT_RATE
#define DEF_REQ_HIT_RATE(f, rate) funs[strings(F_, f)].reqd_hit = rate;
#include "funs.def"
#undef DEF_REQ_HIT_RATE
#define DEF_REQ_HIT_RATE(a, b)  /* empty */
#undef DEF

  while (1) {
    usleep(1000 * mem_stop_int);

    /* Consider stopping memoization */
    for(f=0; f < F_MAX; f++) {
      stopit(&funs[f]);
    }

    /* Consider restarting */
    for(f=0; f < F_MAX; f++) {
      fprintf(stderr, "Restarting for '%s'\n", funs[f].name);
      restart(&funs[f]);
    }
  }
}
#endif  /* !mem_actualtime && mem_stop */


void __attribute__((constructor)) init()
{
  int f;

  fprintf(stderr, "======== Constructor ==========\n");

#ifdef mem_debug
  init_debug();
#endif

  /* Shall we pin the helper thread to a core, or let the system take
     care of this? */
#if !defined(mem_actualtime) && defined(mem_stop)
  if (pthread_create(&thread, NULL, helper, NULL) != 0) {
    DEBUG_PRINTF("Could not create helper thread.\n");
  }
#endif  /* !mem_actualtime && mem_stop */


  /* load symbols of original functions */
  for(f=0; f < F_MAX; f++) {
    funs[f].real_func = dlsym(RTLD_NEXT, funs[f].name);
  }

#ifdef mem_time
  for(f=0; f < F_MAX; f++) {
    char mem_outfile[100];

    snprintf(mem_outfile, sizeof(mem_outfile), "if-memo-%s.txt", funs[f].name);
    funs[f].dumptime = fopen(mem_outfile, "w");
 }
#endif  /* mem_time */

  /* Initialize tables with NaNs. */
  fillarg();
}



void fini_common(FILE* outfile, struct fun_t* f)
{
  if (f->hit + f->i > 0) {
    fprintf(outfile, "function %s\n", f->name);
    fprintf(outfile, "\tnum calls = %lu\n", f->hit + f->i);
    fprintf(outfile, "\tnum hits  = %lu\n", f->hit);
  }
}


void __attribute__((destructor)) fini()
{
#ifdef mem_debug
  int f;

  for(f=0; f < F_MAX; f++) {
    fini_common(debug, &funs[f]);
  }


#ifdef mem_time
  for(f=0; f < F_MAX; f++) {
    timeprint(&funs[f]);
  }
#endif  /* mem_time */

#endif  /* mem_debug */
}
